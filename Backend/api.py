from flask import Flask,jsonify,request
from flask_cors import CORS
import mysql.connector


host = "database.default.svc.cluster.local"
username = "nexo-user"
password = "nexo-user-secret"
database = "devops-demo-task"
conn = mysql.connector.connect(host=host, user=username, password=password, database=database, connect_timeout=60)

api = Flask(__name__)
CORS(api)

@api.route('/health', methods = ['GET'])
def health():
  return "Backend is running"

@api.route('/save-name', methods = ['POST'])
def save():
  cursor = conn.cursor()
  save_query = "INSERT INTO users (first_name, last_name) VALUES ('{0}', '{1}');".format(request.json['first_name'], request.json['last_name'])
  cursor.execute(save_query)
  conn.commit()
  cursor.close()
  return "OK"

@api.route('/get-last-name', methods = ['POST'])
def get_last_name_by_first_name():
  cursor = conn.cursor(dictionary=True)

  get_last_name_by_first_name_query = "SELECT last_name FROM users WHERE first_name = '{0}' LIMIT 1;".format(request.json['first_name'])
  cursor.execute(get_last_name_by_first_name_query)
  row = cursor.fetchone()
  cursor.close()
  return row
