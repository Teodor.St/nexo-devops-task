## Local Setup

From the root directory run:
```
docker compose up --build
```
This will build all containers and start them. Access the frontend on http://localhost:80

## Pipeline
In the repository `CI/CD` tab click on the `Run Pipeline` button. Select the service you want to build. Possible options are:
- Frontend - build and push frontend app
- Backend - build and push backend app
- Database - build and push database
- all - build and push all services

## Images

Images can be found in the `Packages & Registries/Container Registry` tab in the repostiory

## Helm charts

Helm charts are configured to pull images from the gitlab container registry with hardcoded tag. Tag should be updated with the image wanted manually.

## Docker registry Kubernetes secret

In the root of the project is the `docker-registry-secret.yaml` which contains the credentials required to authenticate to the gitlab registry in this project. It contains Teodor.St username and **read-only** API token for my account.

## Concerns

Because of Minikube running with docker provider on OSX with arm64 architecture we are limited for DNS resolution. Therefore it is difficult to connect the Frontend app to the Backend app via the Backend K8S service.
